-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2017 at 09:01 AM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_php`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `username`, `password`) VALUES
(1, 'jithin', 'jithin');

-- --------------------------------------------------------

--
-- Table structure for table `cactegory`
--

CREATE TABLE `cactegory` (
  `id` int(10) NOT NULL,
  `cactegory` varchar(255) NOT NULL,
  `percentage` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cactegory`
--

INSERT INTO `cactegory` (`id`, `cactegory`, `percentage`) VALUES
(1, 'physics', 2),
(2, 'maths', 2),
(3, 'english', 2);

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE `marks` (
  `id` int(10) NOT NULL,
  `part_id` int(10) NOT NULL,
  `mark` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`id`, `part_id`, `mark`) VALUES
(1, 50, 1),
(2, 52, 1),
(3, 53, 1),
(4, 55, 3),
(5, 55, 3),
(6, 56, 2),
(7, 56, 2),
(8, 56, 2),
(9, 56, 2),
(10, 60, 2),
(11, 60, 2);

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`id`, `name`, `email`) VALUES
(1, 'hari', 'hari@e'),
(2, 'hari', 'hari@e'),
(3, 'fgrg', 'fg@fg'),
(4, 'hj', 'gh@fg'),
(27, 'fggfgfg', 'gh@fggfffgfgf'),
(28, 'fggfgfg', 'gh@fggfffgfgf'),
(29, 'jklil', 'jkjhk@ghjj'),
(30, 'hjkh', 'ghg#ghgfh'),
(31, 'hjhj', 'hjjhfdff@www'),
(32, 'ikiku', 'fgrdfg@hygy'),
(33, 'kjlil', 'jhuh#gh'),
(34, 'ghgg', 'dff@fgfgfg'),
(35, 'hjhgjh', 'dfdf@ghgh'),
(36, 'mjkmjm', 'nmbm@ghhhg'),
(37, 'mjkmjmdefredf', 'nmbm@ghhhg'),
(38, 'fdg', 'xc@fd'),
(55, 'shabana', 'shab@s'),
(56, 'vbggvg@bgvn', ' b '),
(57, 'fff', 'f'),
(58, 'yuy', 'uyt'),
(59, 'gfhb', 'fghf'),
(60, 'sad', 'df');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) NOT NULL,
  `cat_id` int(10) NOT NULL,
  `questions` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `option_a` varchar(255) NOT NULL,
  `option_b` varchar(255) NOT NULL,
  `option_c` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `cat_id`, `questions`, `answer`, `option_a`, `option_b`, `option_c`) VALUES
(1, 1, 'what is physics', 'phy', 'qqqq', 'sedd', 'phy'),
(2, 1, 'why physics', 'pppppp', 'efdd', 'pppppp', 'fgg'),
(3, 2, 'what is maths', 'math', 'math', 'gfgdf', 'dgfdgd'),
(4, 2, 'why math', 'mmmmm', 'dfdf', 'ertret', 'mmmmm'),
(5, 3, 'what is english', 'eng', 'eng', 'yuyu', 'erew'),
(6, 3, 'why english', 'eeeeee', 'wew', 'qwwq', 'eeeeee');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cactegory`
--
ALTER TABLE `cactegory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cactegory`
--
ALTER TABLE `cactegory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `marks`
--
ALTER TABLE `marks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
