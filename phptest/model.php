<?php
function open_database_connection() {
    $link = new PDO ("mysql:host=localhost;dbname=test_php",'root','root');
    return $link;
}
function close_database_connection( $link ) {
    $link = null;
}
function  check_login() {
    $link = open_database_connection();
    $stmt = $link-> prepare("SELECT * FROM administrator WHERE username = :username AND password = :pass");
    $stmt->bindParam(":username",$_POST['username']);
    $stmt->bindParam(":pass",$_POST['password']);
    $t = $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    close_database_connection( $link );
    return $row;
}
function insert_student() {
    $link = open_database_connection();
    $stmt = $link-> prepare("INSERT INTO participant( name, email ) VALUES (:name, :email)");
    $stmt->bindParam(":name",$_POST['name']);
    $stmt->bindParam(":email",$_POST['email']);
    $t = $stmt->execute();
    $lid = $link->lastInsertId();
    close_database_connection( $link );
    return $lid;
    
}
function get_questions() {
    $link = open_database_connection();
    $stmt = $link-> prepare("SELECT id, questions, option_a, option_b, option_c FROM questions");
    $t = $stmt->execute();
    while($que = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $questions[] = $que;
    }
    close_database_connection( $link );
    return $questions;
}
function get_answer() {
    $link = open_database_connection();
    $stmt = $link-> prepare("SELECT answer FROM questions WHERE id = :id");
    $stmt->bindParam(":id",$_POST['quest_id']);
    $t = $stmt->execute();
    $answer = $stmt->fetch(PDO::FETCH_ASSOC);
    close_database_connection( $link );
    return $answer;
}
function  insert_part_mark() {
    $link = open_database_connection();
    $stmt = $link-> prepare("INSERT INTO marks( part_id, mark ) VALUES (:partid, :mark)");
    $stmt->bindParam(":partid",$_GET['pid']);
    $stmt->bindParam(":mark",$_GET['mark']);
    $t = $stmt->execute();
    close_database_connection( $link );
}
?>