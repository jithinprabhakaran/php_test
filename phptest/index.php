<?php 
require_once 'controllers.php';
require_once 'model.php';
session_start();
$uri = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH);
if ("/index.php/admin/" == $uri) {
   show_homepage();
}
elseif("index.php/login/" == $uri) {
    login_action();    
}
elseif("/index.php/reg/" == $uri) {
    student_insert();
}
elseif("/index.php/student/" == $uri) {
    student_reg_form();
}
elseif("/index.php/quest/" == $uri) {
    show_questions();
}
elseif("/index.php/addmark" == $uri) {
    insert_marks();
}
?>
